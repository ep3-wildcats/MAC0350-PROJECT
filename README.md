Integrantes do grupo:

André Luiz Akabane Solak (9793572)

Gabriely Rangel Pereira (9793544)

Victor Martins João (9793551)

# COMENTÁRIOS SOBRE OS ERROS:
A versão atual do programa apresenta falhas e não possui todas as funcionalidades que deveria oferecer. Devido a problemas com autenticação do usuaŕio, surgiram erros ao tentar integrar dados do banco através das requisições, o que impediu o funcionamento do conjunto de telas CRUD (as páginas estão implementadas, porém não realizam suas respectivas operações), a restrição ao acesso das páginas e a adição de novos planos. Acerca dos planos, eles podem ser criados, mas não há como carregá-los com os dados reais do banco.

# MAC0350-PROJECT

Template for the project developed during the course MAC0350
(Introduction to Systems Development) at IME-USP.

This repository is a monorepo and requires [docker][1] and
[docker-compose][2] to run the services.

In order to setup the back-end services, open a shell and run:
```bash
cd server
docker-compose up
```

In order to setup the front-end services, open another shell and run:
```bash
cd client
docker-compose up
```

[1]: https://store.docker.com/search?type=edition&offering=community
[2]: https://docs.docker.com/compose/install/ 
