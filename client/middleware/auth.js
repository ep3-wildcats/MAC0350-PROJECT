export default ({ store, route, redirect, $axios }) => {
  console.log(`Processing "${route.fullPath}" in middleware/auth.js`);

  // TODO: Allow access to all main pages!
  if (route.fullPath === '/') {
    console.log(`Page with anonymous access!`);
    return;
  }

  // Use info derived by token from global vuex store
  if (!store.getters.isAuthenticated) {
    console.log(`You are not logged in! Redirecting to safe zone`);
    return redirect('/');
  }

  else {
    // TODO: Verify if user has permission to access this page
    let headers = {
      'Content-Type': 'application/json',
      'Authorization': store.state.token
    };
    
    let data = {
      caminho_servico: route.fullPath
    };

    let res;

    let checkAccess = () => {
      if (!res){
        return redirect('/');
      }
    }

    let setVar = (bla) => {
      res = bla;
      checkAccess();      
    }


    let haveAccess = async () => {
      console.log(data);
      console.log(headers);
      let result = await $axios.$post("/rpc/autorizar", data, headers);
      console.log('result');
      console.log(result);
      result = true;
      setVar(result);
    }
    
    haveAccess(); 
  }

  console.log(`I have a token! ${store.state.token}`);
}
