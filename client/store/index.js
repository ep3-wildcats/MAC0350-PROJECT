import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
})

export default () => {
  return new Vuex.Store({
    plugins: [vuexLocal.plugin],

    state: {
      email: '',
      token: '',
      selectedButton: 0,
      newPlan: [],
      plans: [],
      activeDiscipline: ''
    },

    getters: {
      isAuthenticated(state) {
        return state.token !== '';
      },

      isSelectedButton: (state) => (id) => {
        return state.selectedButton === id;
      },

      getNumPlans(state) {
        return state.plans.length;
      },
      
      whatDiscipline(state, obj) {
        return state.activeDiscipline;
      }
    },

    mutations: {
      setEmail(state, { email }) {
        state.email = email;
      },

      setToken(state, { token }) {
        state.token = token;
      },

      setSelectedButton(state, id){
        state.selectedButton = id;
      },

      resetNewPlan(state) {
        state.newPlan = [];
      },

      updateNewPlan(state, obj) {
        state.newPlan[obj.id_disciplina] = obj;  
      },

      pushPlan(state, obj) {
        state.plans.push(obj);
      },

      resetPlan(state) {
        state.plans = []
        state.newPlan[obj.id_disciplina] = obj;
      },

      setDiscipline(state, obj) {
        state.activeDiscipline = obj;
      }
    }
  });
}
