CREATE OR REPLACE FUNCTION
public.modulo_disciplina(id_modulo integer)
  RETURNS public.disciplinas
  LANGUAGE SQL
AS $$
    SELECT disciplinas.*
     FROM optativas_compoem_modulos JOIN disciplinas USING (id_disciplina)
    WHERE optativas_compoem_modulos.id_modulo = modulo_disciplina.id_modulo;
$$;